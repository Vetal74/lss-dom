$(function () {
    $('.slick').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: '<div class="arrow next">&gt;</div>',
        prevArrow: '<div class="arrow prev">&lt;</div>'
    });

    $('ul.tabs__caption').each(function() {
        $(this).find('li').each(function(i) {
            $(this).click(function(){
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('div.tabs').find('div.tabs__content').removeClass('active').eq(i).addClass('active');
            });
        });
    });

    var openModal = function() {
        $(".modal").addClass("active");
        $(".layoutModal").addClass("active");
    };
    var closeModal = function() {
        $(".modal").removeClass("active");
        $(".layoutModal").removeClass("active");
    };

    $(".block-6 .button").click(function() {
        openModal();
        return false;
    });
    $(".layoutModal").click(closeModal);

});
